package io.fortumo;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

@FunctionalInterface
public interface ResultHandler {
    void handleResult(ResultSet resultSet) throws SQLException, IOException;
}
