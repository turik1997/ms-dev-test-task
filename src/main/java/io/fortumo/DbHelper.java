package io.fortumo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class DbHelper {

    private DBPool pool = new DBPool();

    public DbHelper(DBPool pool) {
        this.pool = pool;
    }

    protected Connection getConnection() {
        return pool.getConnection();
    }

    protected void release(Connection c) {
        pool.release(c);
    }

    public void runQuery(ResultHandler handler, String query, Object... params) {
        final Connection connection = this.getConnection();
        try {
            final PreparedStatement prepared = connection.prepareStatement(query);
            for (int i = 0; i < params.length; i++) {
                insertParameter(prepared, params[i], i+1);
            }
            System.out.println("Executing " + prepared.toString());
            final ResultSet result = prepared.executeQuery();
            handler.handleResult(result);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            release(connection);
        }
    }

    abstract protected void insertParameter(PreparedStatement statement, Object param, int index) throws SQLException;
}
