package io.fortumo.report;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DateRange {

    private LocalDate from;
    private LocalDate to;

    public DateRange(String start, String end) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
        from = LocalDate.parse(start, formatter);
        to = LocalDate.parse(end, formatter);
    }

    public LocalDate getFrom() {
        return from;
    }

    public LocalDate getTo() {
        return to;
    }
}
