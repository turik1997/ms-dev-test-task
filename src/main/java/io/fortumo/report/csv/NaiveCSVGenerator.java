package io.fortumo.report.csv;

import io.fortumo.DbHelper;
import io.fortumo.report.ReportGenerator;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class NaiveCSVGenerator implements ReportGenerator {

    private final static String NEW_LINE = System.getProperty("line.separator");
    private final static int BUF_SIZE = 4096;
    private DbHelper db;
    private final Calendar calendar;

    public NaiveCSVGenerator(DbHelper db) {
        this.db = db;
        this.calendar = Calendar.getInstance(); // creates calendar
    }

    private Date addOneDay(Date date) {
        synchronized (calendar) {
            calendar.setTime(date);
            calendar.add(Calendar.HOUR_OF_DAY, 24);
            return calendar.getTime();
        }
    }

    @Override
    public void generate(String merchant, String dateFrom, String dateTo, OutputStream os) {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date from = null;
        Date until = null;
        try {
            from = dateFormat.parse(dateFrom);
            until = addOneDay(dateFormat.parse(dateTo));
        } catch(Exception e) {
            try {
                os.flush();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return;
        }

        db.runQuery((r) -> {
                    ResultSetMetaData metaData = r.getMetaData();
                    String[] columnNames = getColumnNames(metaData);
                    int[] columnTypes = getColumnTypes(metaData);
                    int columnCount = columnNames.length;
                    sb.append(buildTableTitle(columnNames));
                    String rowFormat = buildRowFormat(columnTypes);
                    //System.out.println("Title: " + sb.toString());
                    //System.out.println("Row format: " + rowFormat);

                    Object[] buf = new Object[columnCount];
                    while (r.next()) {
                        for ( int i = 0; i < columnCount; i++ ) {
                            buf[i] = r.getObject(i+1);
                        }
                        sb.append(String.format(rowFormat, buf));
                        if (sb.length() > BUF_SIZE) {
                            os.write(sb.toString().getBytes());
                            sb.setLength(0);
                        }
                    }
                    if (sb.length() > 0) os.write(sb.toString().getBytes());
                }, "SELECT * FROM payments "
                        + "WHERE merchant_uuid = COALESCE(?, merchant_uuid) "
                        + "AND created_at >= ? "
                        + "AND created_at < ?",
                merchant, new Timestamp(from.getTime()), new Timestamp(until.getTime()));
    }

    private String buildRowFormat(int[] columnTypes) {
        StringBuilder rowFormatBuilder = new StringBuilder();
        for ( int i = 0; i < columnTypes.length; i++ ) {
            if (columnTypes[i] == Types.INTEGER)
                rowFormatBuilder.append("%d");
            else if (columnTypes[i] == Types.TIMESTAMP) {
                rowFormatBuilder.append("%s");
            } else if (columnTypes[i] == Types.VARCHAR) {
                rowFormatBuilder.append("%s");
            } else if (columnTypes[i] == Types.NUMERIC) {
                rowFormatBuilder.append("%f");
            }
            if (i == columnTypes.length-1) rowFormatBuilder.append(NEW_LINE);
            else rowFormatBuilder.append(", ");
        }
        return rowFormatBuilder.toString();
    }

    private int[] getColumnTypes(ResultSetMetaData metaData) throws SQLException {
        int[] types = new int[metaData.getColumnCount()];
        for ( int i = 0; i < types.length; i++ ) {
            types[i] = metaData.getColumnType(i+1);
        }
        return types;
    }

    private String[] getColumnNames(ResultSetMetaData metaData) throws SQLException {
        StringBuilder sb = new StringBuilder();
        int columnCount = metaData.getColumnCount();
        String columnNames[] = new String[columnCount];
        for ( int i = 0; i < columnCount-1; i++ ) {
            columnNames[i] = metaData.getColumnName(i+1);
        }
        columnNames[columnCount-1] = metaData.getColumnName(columnCount);
        return columnNames;
    }

    private String buildTableTitle(String[] columns) {
        StringBuilder titleFormat = new StringBuilder();
        for ( int i = 0; i < columns.length-1; i++ ) {
            titleFormat.append("%s, ");
        }
        titleFormat.append("%s").append(NEW_LINE);
        return String.format(titleFormat.toString(), columns);
    }
}
