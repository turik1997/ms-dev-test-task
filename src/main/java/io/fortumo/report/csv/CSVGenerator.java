package io.fortumo.report.csv;

import io.fortumo.CommonDbHelper;
import io.fortumo.report.DateRange;
import io.fortumo.report.ReportGenerator;

import java.io.OutputStream;
import java.util.Comparator;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

public class CSVGenerator implements ReportGenerator {

    private CommonDbHelper dbHelper;
    private Map<String, TreeMap<DateRange, Queue>>  cache;

    @Override
    public void generate(String merchant, String dateFrom, String dateTo, OutputStream os) {

        DateRange dateRange = new DateRange(dateFrom, dateTo);
        synchronized (cache) {
            TreeMap<DateRange, Queue> searchRanges = cache.get(merchant);
            if (searchRanges == null) {
                searchRanges = new TreeMap<DateRange, Queue>(Comparator.comparing(DateRange::getFrom));
                cache.put(merchant, searchRanges);
            }
            DateRange closestLeft = searchRanges.floorKey(dateRange);
            DateRange closestRight;
        }

        dbHelper.runQuery((r) -> {
            /*while (r.next()) {
                os.write("".getBytes());
            }*/
        }, "SELECT * FROM payments"
            + "WHERE (merchant_uuid = ? "
            + "AND created_at >= ? "
            + "AND created_at <= ?);",
            merchant, dateFrom, dateTo);
    }
}
