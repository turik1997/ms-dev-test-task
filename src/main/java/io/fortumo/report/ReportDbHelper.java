package io.fortumo.report;

import io.fortumo.DBPool;
import io.fortumo.DbHelper;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

public class ReportDbHelper extends DbHelper {

    public ReportDbHelper(DBPool pool) {
        super(pool);
    }

    @Override
    protected void insertParameter(PreparedStatement statement, Object param, int index) throws SQLException {
        switch (index) {
            case 1:
                statement.setString(index, (String)param);
                break;
            case 2:
            case 3:
                statement.setTimestamp(index, (Timestamp)param);
                break;
        }
    }
}
