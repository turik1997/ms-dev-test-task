package io.fortumo.report;

import java.io.OutputStream;

public interface ReportGenerator {
    void generate(String merchant, String dateFrom, String dateTo, OutputStream os);
}
