package io.fortumo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.Stack;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class DBPool {

    private int size;
    private BlockingQueue<Connection> pool;

    private Connection createConnection() throws Exception {
        final String url = "jdbc:postgresql://127.0.0.1/fortumo_test_task";
        final Properties props = new Properties();
        props.setProperty("user","fortumo_test_user");
        props.setProperty("password","fortumo_test_password");
        props.setProperty("ssl","false");
        final Connection conn = DriverManager.getConnection(url, props);

        return conn;
    }

    public void create(int size) {
        this.size = size;
        pool = new LinkedBlockingQueue<>(size);
        for ( int i = 0; i < size; i++ ) {
            try {
                pool.add(createConnection());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Connection getConnection() {
        synchronized (this) {
            while (pool.isEmpty()) {
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return pool.poll();
    }

    public void release(Connection connection) {
        if (pool.size() == size) return;
        synchronized (this) {
            try {
                if (connection.isClosed()) {
                    pool.add(createConnection());
                    this.notify();
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            pool.add(connection);
            this.notify();
        }
    }
}
