package io.fortumo;

import java.sql.*;
import java.util.Properties;

public class CommonDbHelper extends DbHelper {

    public CommonDbHelper(DBPool pool) {
        super(pool);
    }

    @Override
    protected void insertParameter(PreparedStatement statement, Object param, int index) throws SQLException {
        statement.setString(index, (String)param);
    }
}
