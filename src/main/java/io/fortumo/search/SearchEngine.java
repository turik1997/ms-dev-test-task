package io.fortumo.search;

public interface SearchEngine {

    String search(String merchant, String msisdn, String country, String operator);
}
