package io.fortumo.search;

import io.fortumo.CommonDbHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

public class SharedSearcher implements SearchEngine {

    private Map<String, Future<String>> runningSearches;
    private final ExecutorService executorService;
    private CommonDbHelper commonDbHelper;
    public SharedSearcher(CommonDbHelper commonDbHelper) {
        this.commonDbHelper = commonDbHelper;
        this.runningSearches = new HashMap<>();
        this.executorService = new ThreadPoolExecutor(5, 10, Integer.MAX_VALUE, TimeUnit.MICROSECONDS, new LinkedBlockingQueue<>(10));
    }

    @Override
    public String search(String merchant, String msisdn, String country, String operator) {
        int keyLength = (merchant == null ? 0 : merchant.length())
                    + (msisdn == null ? 0 : msisdn.length())
                    + (country == null ? 0: country.length())
                    + (operator == null ? 0 : operator.length());
        StringBuilder sb = new StringBuilder(keyLength);
        sb.append(merchant).append(msisdn).append(country).append(operator);
        String key = sb.toString();
        Future<String> searchResult = null;
        synchronized (runningSearches) {
            searchResult = runningSearches.get(key);
            if (searchResult == null) {
                searchResult = executorService.submit(new SearchTask(merchant, msisdn, country, operator, commonDbHelper));
                runningSearches.put(key, searchResult);
            }
        }

        String result = null;
        try {
           result = searchResult.get();
           synchronized (runningSearches) {
               runningSearches.remove(key); //possible to replace with time-based map with expiring keys (aka cache?)
           }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return result;
    }
}
