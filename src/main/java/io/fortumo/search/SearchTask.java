package io.fortumo.search;

import io.fortumo.CommonDbHelper;
import java.sql.SQLException;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

public class SearchTask implements Callable<String> {

    private String merchant;
    private String operator;
    private String country;
    private String msisdn;
    private CommonDbHelper db;

    public SearchTask(String merchant, String msisdn, String country, String operator, CommonDbHelper db) {
        this.merchant = merchant;
        this.operator = operator;
        this.country = country;
        this.msisdn = msisdn;
        this.db = db;
    }

    @Override
    public String call() throws Exception {
        final AtomicReference<String> theResult = new AtomicReference<>();
        this.db.runQuery((r) -> {
                try {
                    if ( !r.next() ) {
                        theResult.set("No records with merchant=" + merchant);
                        return;
                    }
                    final String id = r.getString("id");
                    final String createdAt = r.getTimestamp("created_at").toLocalDateTime().toString();
                    theResult.set("Found record " + id + " created at " + createdAt + "\n");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }, "SELECT * FROM payments "
                    + "WHERE merchant_uuid = COALESCE(?, merchant_uuid) "
                    + "AND msisdn = COALESCE(?, msisdn) "
                    + "AND country_code = COALESCE(?, country_code) "
                    + "AND operator_code = COALESCE(?, operator_code) "
                    + " LIMIT 1",
            merchant,
            msisdn,
            country,
            operator);
        return theResult.get();
    }
}
