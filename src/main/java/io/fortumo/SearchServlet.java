package io.fortumo;

import io.fortumo.report.ReportDbHelper;
import io.fortumo.report.csv.NaiveCSVGenerator;
import io.fortumo.report.ReportGenerator;
import io.fortumo.search.SearchEngine;
import io.fortumo.search.SharedSearcher;

import java.io.IOException;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SearchServlet extends HttpServlet {

    private SearchEngine searchEngine;
    private ReportGenerator reportGenerator;
    private Calendar today = Calendar.getInstance();

    @Override
    public void init() throws ServletException {
        super.init();
        final DBPool dbPool = new DBPool();
        dbPool.create(10);
        this.searchEngine = new SharedSearcher(new CommonDbHelper(dbPool));
        this.reportGenerator = new NaiveCSVGenerator(new ReportDbHelper(dbPool));
        today.set(Calendar.HOUR_OF_DAY, 0);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logRequestStart(req);
        final String merchant = req.getParameter("merchant");
        final String msisdn = req.getParameter("msisdn");
        final String country = req.getParameter("country");
        final String operator = req.getParameter("operator");
        String fromDate = req.getParameter("from");
        String toDate = req.getParameter("to");
        if (fromDate != null && toDate == null) toDate = String.format("%d-%d-%d", today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH));
        System.out.println(fromDate + " to " + toDate);

        //no date - single entry request
        if (fromDate == null) {
            final String searchResult = this.searchEngine.search(merchant,msisdn,country, operator);
            if (searchResult != null) {
                resp.setContentLength(searchResult.getBytes().length);
                resp.getWriter().write(searchResult);
            }
        } else if (merchant != null) {
            resp.setHeader("Content-Disposition", "attachment;filename=report.csv");
            resp.setContentType("text/csv");
            reportGenerator.generate(merchant, fromDate, toDate, resp.getOutputStream());
        }

        logRequestEnd(req);
    }

    private void logRequestEnd(HttpServletRequest req) {
        this.log("Request end " + req + " from " + req.getRemoteAddr() + ":" + req.getRemotePort());
    }

    private void logRequestStart(HttpServletRequest req) {
        this.log("Request start " + req + " from " + req.getRemoteAddr() + ":" + req.getRemotePort());
    }
}
